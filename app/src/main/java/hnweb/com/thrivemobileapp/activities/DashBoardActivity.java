package hnweb.com.thrivemobileapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import hnweb.com.thrivemobileapp.R;
import hnweb.com.thrivemobileapp.utility.Logout;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.accountSummeryTV:
                intent = new Intent(this, TutorProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.attendenceReportTV:
                intent = new Intent(this, AttendenceReportActivity.class);

                startActivity(intent);

                break;
            case R.id.studentsTV:
                intent = new Intent(this, StudentsListActivity.class);
                startActivity(intent);

                break;
            case R.id.inoutAttendenceTV:
                intent = new Intent(this, InOutAttendenceActivity.class);
                intent.putExtra("StudentId","");
                startActivity(intent);
                break;
            case R.id.downloadFormTV:
                intent = new Intent(this, DownloadFormActivity.class);
                startActivity(intent);
                break;
            case R.id.timeCardTV:
                intent = new Intent(this, TimeCardActivity.class);
                startActivity(intent);
                break;
            case R.id.logoutBTN:
                Logout.logout(this);
                break;
            case R.id.progrssLL:
                intent = new Intent(this,ProgressReportUploadActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
