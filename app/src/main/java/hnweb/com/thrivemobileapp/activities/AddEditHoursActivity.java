package hnweb.com.thrivemobileapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import hnweb.com.thrivemobileapp.R;
import hnweb.com.thrivemobileapp.application.AppAPI;
import hnweb.com.thrivemobileapp.application.MainApplication;
import hnweb.com.thrivemobileapp.utility.LoadingDialog;
import hnweb.com.thrivemobileapp.utility.ToastUlility;
import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;

public class AddEditHoursActivity extends AppCompatActivity implements View.OnClickListener {

    public HintSpinner<String> defaultHintSpinner;

    Spinner hhSS, mmSS, ampmSS, hhES, mmES, ampmES, subjectS;
    boolean hhS = false, mmS = false, ampmS = false, hhE = false, mmE = false, ampmE = false, subjectB = false;
    String[] hhInSpinner = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    String[] mmSpinner = {"00", "15", "30", "45"};
    String[] ampmSpinner = {"AM", "PM"};
    String[] hhOutSpinner = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
    SharedPreferences sharedPreferences;
    private LoadingDialog loadingDialog;
    ArrayList<String> hhInSpinnerList = new ArrayList<String>(Arrays.asList(hhInSpinner));
    ArrayList<String> hhOutSpinnerList = new ArrayList<String>(Arrays.asList(hhOutSpinner));
    ArrayList<String> mmSpinnerList = new ArrayList<String>(Arrays.asList(mmSpinner));
    ArrayList<String> ampmSpinnerList = new ArrayList<String>(Arrays.asList(ampmSpinner));
    String dateSelected, StudentId;
    EditText commentsTV;
    ArrayList<String> subjectIdList = new ArrayList<String>();
    ArrayList<String> subjectNameList = new ArrayList<String>();
    ArrayList<String> subjectRateList = new ArrayList<String>();
    String comeFrom, hrsLeft;
    String attendanceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_hours);

        comeFrom = getIntent().getExtras().getString("ComeFrom");
        dateSelected = getIntent().getExtras().getString("date");
        StudentId = getIntent().getExtras().getString("StudentId");
        hrsLeft = getIntent().getExtras().getString("HrsLeft");

        Log.e("DATE", dateSelected);
        Log.e("StudentId", StudentId);


        hhSS = (Spinner) findViewById(R.id.hhSS);
        mmSS = (Spinner) findViewById(R.id.mmSS);
        ampmSS = (Spinner) findViewById(R.id.ampmSS);
        hhES = (Spinner) findViewById(R.id.hhES);
        mmES = (Spinner) findViewById(R.id.mmES);
        ampmES = (Spinner) findViewById(R.id.ampmES);
        subjectS = (Spinner) findViewById(R.id.subjectS);
        commentsTV = (EditText) findViewById(R.id.commentsTV);

        loadingDialog = new LoadingDialog(this);
        sharedPreferences = getApplicationContext().getSharedPreferences(getPackageName(), 0);

        setSpinners(hhSS, hhInSpinnerList, "InTime(Hr)", "hhS");
        setSpinners(mmSS, mmSpinnerList, "Min:", "mmS");
        setSpinners(ampmSS, ampmSpinnerList, "AM/PM", "ampmS");

        setSpinners(hhES, hhOutSpinnerList, "OutTime(Hr)", "hhE");
        setSpinners(mmES, mmSpinnerList, "Min:", "mmE");
        setSpinners(ampmES, ampmSpinnerList, "AM/PM", "ampmE");
//        setSpinners(hhSS,hhSpinner);

        getSubjectList();


    }

    public void ifModify() {
        if (comeFrom.equals("Modify")) {
            try {
                JSONObject jobj = new JSONObject(StudentId);
                JSONArray jarr = jobj.getJSONArray("response");
                String studentID = jarr.getJSONObject(0).getString("StudentID");
                String HourIn = jarr.getJSONObject(0).getString("HourIn");
                String HourOut = jarr.getJSONObject(0).getString("HourOut");
                String SubjectTaught = jarr.getJSONObject(0).getString("SubjectTaught");
                String Comments = jarr.getJSONObject(0).getString("Comments");
                attendanceId = jarr.getJSONObject(0).getString("AttendanceID");

                String[] startTime = (HourIn.replace(" ", ":")).split(":");
                String[] endTime = (HourOut.replace(" ", ":")).split(":");

                this.StudentId = studentID;

                hhSS.setSelection(hhInSpinnerList.indexOf(startTime[0]));
                hhES.setSelection(hhOutSpinnerList.indexOf(endTime[0]));
                mmSS.setSelection(mmSpinnerList.indexOf(startTime[1]));
                mmES.setSelection(mmSpinnerList.indexOf(endTime[1]));
                ampmSS.setSelection(ampmSpinnerList.indexOf(startTime[3]));
                ampmES.setSelection(ampmSpinnerList.indexOf(endTime[3]));

                subjectS.setSelection(subjectNameList.indexOf(SubjectTaught));
                commentsTV.setText(Comments);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void setSpinners(Spinner hhSS, ArrayList<String> hhSpinner, String hint, final String selected) {


        defaultHintSpinner = new HintSpinner<>(
                hhSS,
                // Default layout - You don't need to pass in any layout id, just your hint text and
                // your list data
                new HintAdapter<String>(AddEditHoursActivity.this, hint, hhSpinner),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                        // Here you handle the on item selected event (this skips the hint selected
                        // event)
                        if (selected.equalsIgnoreCase("hhS")) {
                            hhS = true;

                        } else if (selected.equalsIgnoreCase("mmS")) {
                            mmS = true;
                        } else if (selected.equalsIgnoreCase("ampmS")) {
                            ampmS = true;
                        } else if (selected.equalsIgnoreCase("hhE")) {
                            hhE = true;
                        } else if (selected.equalsIgnoreCase("mmE")) {
                            mmE = true;
                        } else if (selected.equalsIgnoreCase("ampmE")) {
                            ampmE = true;
                        }

                        Log.e("POSTITIONNNNN", String.valueOf(position));
//                                        showSelectedItem(itemAtPosition);
                    }

                });
        defaultHintSpinner.init();


//        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddEditHoursActivity.this, android.R.layout.simple_spinner_item, hhSpinner);
//
//        // Drop down layout style - list view with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        // attaching data adapter to spinner
//        hhSS.setAdapter(dataAdapter);
    }

    public void getSubjectList() {
        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppAPI.getSubjectList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RESPONSE", response);
                JSONObject jobj = null;
                try {
                    jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    if (message_code == 1) {
//                        settingsDialog.dismiss();
                        loadingDialog.dismiss();

                        subjectNameList.clear();
                        subjectNameList.add("Subject");
                        subjectIdList.clear();
                        subjectIdList.add("-1");
                        subjectRateList.clear();
                        subjectRateList.add("-1");
                        JSONArray jarr = jobj.getJSONArray("response");
                        for (int i = 0; i < jarr.length(); i++) {

                            subjectIdList.add(jarr.getJSONObject(i).getString("SubjectID"));
                            subjectNameList.add(jarr.getJSONObject(i).getString("SubjectName"));
                            subjectRateList.add(jarr.getJSONObject(i).getString("SubjectID"));
                        }
//
                        defaultHintSpinner = new HintSpinner<>(
                                subjectS,
                                // Default layout - You don't need to pass in any layout id, just your hint text and
                                // your list data
                                new HintAdapter<String>(AddEditHoursActivity.this, "Select Subject", subjectNameList),
                                new HintSpinner.Callback<String>() {
                                    @Override
                                    public void onItemSelected(int position, String itemAtPosition) {
                                        // Here you handle the on item selected event (this skips the hint selected
                                        // event)
                                        subjectB = true;
//                                        showSelectedItem(itemAtPosition);
                                    }
                                });
                        defaultHintSpinner.init();
//                        // Creating adapter for spinner
//                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddEditHoursActivity.this, android.R.layout.simple_spinner_item, subjectNameList);
//
//                        // Drop down layout style - list view with radio button
//                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//                        // attaching data adapter to spinner
//                        subjectS.setAdapter(dataAdapter);
                        if (comeFrom.equals("Modify")) {
                            ifModify();
                        }

//                        studentsRV.setAdapter(new StudentListAdapter(studentListArrayList, StudentsListActivity.this));

                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TutorID", sharedPreferences.getString("TutorID", ""));

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "subject_list";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void addInOutAttendance() {


        final String hourIn = hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim() + ":" +
                mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00" + " " + ampmSpinnerList.get(ampmSS.getSelectedItemPosition()).toString().trim();

        final String hourOut = hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim() + ":" +
                mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00" + " " + ampmSpinnerList.get(ampmES.getSelectedItemPosition()).toString().trim();

        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppAPI.addInOutAttendance, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RESPONSE", response);
                JSONObject jobj = null;
                try {
                    jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    if (message_code == 1) {
//                        settingsDialog.dismiss();
                        loadingDialog.dismiss();
                        String message = jobj.getString("message");
                        ToastUlility.show(AddEditHoursActivity.this, message);
                        onBackPressed();
//                        finish();
//                        Intent intent = new Intent(AddEditHoursActivity.this, InOutAttendenceActivity.class);
//                        startActivity(intent);
//                        finish();


                    } else {
                        loadingDialog.dismiss();
                        String message = jobj.getString("message");
                        ToastUlility.show(AddEditHoursActivity.this, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("TutorID", sharedPreferences.getString("TutorID", ""));
                params.put("StudentID", StudentId);
                params.put("StartDT", dateSelected);
                params.put("EndDT", dateSelected);
                params.put("TotalHours", diffTime());
                params.put("PayRate", subjectRateList.get(subjectS.getSelectedItemPosition()).toString().trim());
                params.put("TotalPay", totalPay(subjectRateList.get(subjectS.getSelectedItemPosition()).toString().trim()));
                params.put("Comments", commentsTV.getText().toString().trim());
                params.put("SubjectTaught", subjectNameList.get(subjectS.getSelectedItemPosition()).toString().trim());
                params.put("HourIn", hourIn);
                params.put("HourOut", hourOut);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "add_in_out_attendance";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public String diffTime() {
        final String inTime, outTime;
        String time = "";
        if (ampmSpinnerList.get(ampmSS.getSelectedItemPosition()).toString().trim().equals("PM")) {
            if (Integer.parseInt(hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim()) == 12) {
                inTime = (Integer.parseInt(hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim())) + ":" +
                        mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00";
            } else {
                inTime = (Integer.parseInt(hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim()) + 12) + ":" +
                        mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00";
            }

        } else {
            if (Integer.parseInt(hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim()) == 12) {
                inTime = Integer.parseInt(hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim()) + 12 + ":" +
                        mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00";
            } else {
                inTime = hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim() + ":" +
                        mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00";
            }

        }

        if (ampmSpinnerList.get(ampmES.getSelectedItemPosition()).toString().trim().equals("PM")) {
            if (Integer.parseInt(hhInSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim()) == 12) {
                outTime = (Integer.parseInt(hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim())) + ":" +
                        mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim() + ":" + "00";
            } else {
                outTime = (Integer.parseInt(hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim()) + 12) + ":" +
                        mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim() + ":" + "00";
            }

        } else {
            if (Integer.parseInt(hhInSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim()) == 12) {
                outTime = (Integer.parseInt(hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim()) + 12) + ":" +
                        mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim() + ":" + "00";
            } else {
                outTime = hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim() + ":" +
                        mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim() + ":" + "00";
            }

        }

        Log.e("INT", inTime);
        Log.e("OUTT", outTime);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(inTime);
            d2 = format.parse(outTime);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();
            long diffHours = diff / (60 * 60 * 1000) % 24;

            time = TimeUnit.MILLISECONDS.toHours(diff) + ":" +
                    (TimeUnit.MILLISECONDS.toMinutes(diff) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff)));

            Log.e("test", time + " hours, ");
        } catch (Exception e) {
            // TODO: handle exception
        }

        return time;
    }

    public String totalPay(String payRate) {

        String diff = diffTime().replace(":", ".");
        Log.e("difftotal", diff);

        double total = Double.parseDouble(diff) * Double.parseDouble(payRate);

        Log.e("total", String.valueOf(total));
        return String.valueOf(total);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitBTN:
                try {

                    if (!hhS) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select InTime(Hr)");
                    } else if (!mmS) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select start time Min:");
                    } else if (!ampmS) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select start AM/PM");
                    } else if (!hhE) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select OutTime(Hr)");
                    } else if (!mmE) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select end time Min:");
                    } else if (!ampmE) {
                        ToastUlility.show(AddEditHoursActivity.this, "Please select end AM/PM");
                    } else if (!subjectB) {
                        ToastUlility.show(this, "You must select a subject.");
                    } else {
                        if (hhSS.getSelectedItem().toString().equalsIgnoreCase("InTime(Hr)")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select InTime(Hr)");
                        } else if (hhES.getSelectedItem().toString().equalsIgnoreCase("OutTime(Hr)")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select OutTime(Hr)");
                        } else if (mmSS.getSelectedItem().toString().equalsIgnoreCase("Min:")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select start time Min:");
                        } else if (mmES.getSelectedItem().toString().equalsIgnoreCase("Min:")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select end time Min:");
                        } else if (ampmSS.getSelectedItem().toString().equalsIgnoreCase("AM/PM")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select start AM/PM");
                        } else if (ampmES.getSelectedItem().toString().equalsIgnoreCase("AM/PM")) {
                            ToastUlility.show(AddEditHoursActivity.this, "Please select end AM/PM");
                        } else {
                            String subject = subjectS.getSelectedItem().toString().trim();

                            int outSpin = Integer.parseInt(hhOutSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim());
                            int inSpin = Integer.parseInt(hhInSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim());

                            if ((ampmSpinnerList.get(ampmSS.getSelectedItemPosition()).toString().trim())
                                    .equalsIgnoreCase((ampmSpinnerList.get(ampmES.getSelectedItemPosition()).toString().trim()))) {
                                if (inSpin > outSpin) {
//                            submit();
                                    try {
                                        if (subject.equalsIgnoreCase("")) {
                                            ToastUlility.show(this, "You must select a subject.");
                                        } else if (subject.equalsIgnoreCase("Select Student")) {
                                            ToastUlility.show(this, "You must select a subject.");
                                        } else {
//                                    ToastUlility.show(this, subjectS.getSelectedItem().toString());

                                            submit();
                                        }

                                    } catch (Exception e) {
                                        ToastUlility.show(this, "You must select a subject.");
                                    }

                                } else {
                                    if (inSpin == outSpin) {
                                        if (Integer.parseInt(mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim()) <
                                                Integer.parseInt(mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim())) {

                                            try {
                                                if (subjectS.getSelectedItem().toString().equalsIgnoreCase("")) {
                                                    ToastUlility.show(this, "You must select a subject.");
                                                } else if (subjectS.getSelectedItem().toString().equalsIgnoreCase("Select Student")) {
                                                    ToastUlility.show(this, "You must select a subject.");
                                                } else {
//                                    ToastUlility.show(this, subjectS.getSelectedItem().toString());

                                                    submit();
                                                }

                                            } catch (Exception e) {
                                                ToastUlility.show(this, "You must select a subject.");
                                            }

                                        } else {
                                            ToastUlility.show(AddEditHoursActivity.this, "Selected out time minutes less than in time minutes");
                                        }
                                    } else {
                                        ToastUlility.show(AddEditHoursActivity.this, "Selected out time less than in time.");
                                    }
                                }
                            } else if ((ampmSpinnerList.get(ampmSS.getSelectedItemPosition()).toString().trim())
                                    .equalsIgnoreCase("AM") && (ampmSpinnerList.get(ampmES.getSelectedItemPosition()).toString().trim())
                                    .equalsIgnoreCase("PM")) {
                                submit();
                            } else {
                                ToastUlility.show(AddEditHoursActivity.this, "Please correct the AM/PM time stamp.");
                            }

                        }
                    }
                } catch (Exception e) {
                    ToastUlility.show(AddEditHoursActivity.this, "You must select Time and Subject");
                }


                break;
        }
    }

    public void submit() {
        String diff = diffTime().replace(":", ".");
        //totalPay(subjectRateList.get(subjectS.getSelectedItemPosition()).toString().trim());
        if (comeFrom.equals("Modify")) {

            if (Double.parseDouble(diff) < Double.parseDouble(hrsLeft)) {
                modifyInOutAttendance();
            } else {
                ToastUlility.show(AddEditHoursActivity.this, "Only " + hrsLeft + " left of this student");
            }

        } else if (comeFrom.equals("ADD")) {

            if (Double.parseDouble(diff) < Double.parseDouble(hrsLeft)) {
                addInOutAttendance();
            } else {
                ToastUlility.show(AddEditHoursActivity.this, "Only " + hrsLeft + " left of this student");
            }

        }
    }

    public void modifyInOutAttendance() {


        final String hourIn = hhInSpinnerList.get(hhSS.getSelectedItemPosition()).toString().trim() + ":" +
                mmSpinnerList.get(mmSS.getSelectedItemPosition()).toString().trim() + ":" + "00" + " " + ampmSpinnerList.get(ampmSS.getSelectedItemPosition()).toString().trim();

        final String hourOut = hhOutSpinnerList.get(hhES.getSelectedItemPosition()).toString().trim() + ":" +
                mmSpinnerList.get(mmES.getSelectedItemPosition()).toString().trim() + ":" + "00" + " " + ampmSpinnerList.get(ampmES.getSelectedItemPosition()).toString().trim();

        loadingDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppAPI.modifyAttendance, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RESPONSE", response);
                JSONObject jobj = null;
                try {
                    jobj = new JSONObject(response);
                    int message_code = jobj.getInt("message_code");
                    if (message_code == 1) {
//                        settingsDialog.dismiss();
                        loadingDialog.dismiss();
                        String message = jobj.getString("message");
                        ToastUlility.show(AddEditHoursActivity.this, message);
                        onBackPressed();
//                        finish();
//                        Intent intent = new Intent(AddEditHoursActivity.this, InOutAttendenceActivity.class);
//                        startActivity(intent);
//                        finish();


                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                ToastUlility.show(getApplicationContext(), "Network Error,please try again");
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("AttendanceID", attendanceId);
//                params.put("StudentID", StudentId);
//                params.put("StartDT", dateSelected);
//                params.put("EndDT", dateSelected);
                params.put("TotalHours", diffTime());
                params.put("PayRate", subjectRateList.get(subjectS.getSelectedItemPosition()).toString().trim());
                params.put("TotalPay", totalPay(subjectRateList.get(subjectS.getSelectedItemPosition()).toString().trim()));
                params.put("Comments", commentsTV.getText().toString().trim());
                params.put("SubjectTaught", subjectNameList.get(subjectS.getSelectedItemPosition()).toString().trim());
                params.put("HourIn", hourIn);
                params.put("HourOut", hourOut);

                Log.e("PARAMS", params.toString());
                return params;

            }
        };
        String request_tag = "modify_in_out_attendance";
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddEditHoursActivity.this, InOutAttendenceActivity.class);
        intent.putExtra("StudentId", StudentId);
        startActivity(intent);
        finish();

//        super.onBackPressed();
//        Intent intent = new Intent()
    }
}
